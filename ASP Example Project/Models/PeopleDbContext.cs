﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ASP_Example_Project.Models
{
    public class PeopleDbContext : DbContext
    {
        public PeopleDbContext() : base("DefaultConnection")
        {

        }

        public DbSet<PersonModel> People { get; set; }
    }
}