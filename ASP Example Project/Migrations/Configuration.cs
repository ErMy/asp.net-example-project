namespace ASP_Example_Project.Migrations
{
    using ASP_Example_Project.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ASP_Example_Project.Models.PeopleDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ASP_Example_Project.Models.PeopleDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            var people = new List<PersonModel>
            {
                new PersonModel {FirstName = "Sascha", LastName = "Derm"},
                 new PersonModel {FirstName = "Dino", LastName = "Tell"},
                  new PersonModel {FirstName = "Sarah", LastName = "Lervi"},
                   new PersonModel {FirstName = "Joshua", LastName = "Salk"}
            };
            people.ForEach(x => context.People.AddOrUpdate(p => p.LastName, x));
            context.SaveChanges();
        }
    }
}
